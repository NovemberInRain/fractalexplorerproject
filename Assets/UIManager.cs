﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public delegate void OnCbValueChangedDelegate(Dropdown change);    
    public OnCbValueChangedDelegate cbFractalValueChangedDelegate;
    public Dropdown cbFractal;
    public OnCbValueChangedDelegate cbPresetValueChangedDelegate;
    public Dropdown cbPreset;

    public delegate void OnSliderValueChangedDelegate(Slider change);
    public OnSliderValueChangedDelegate saturationValueChangedDelegate;
    public Slider sSaturation;
    public OnSliderValueChangedDelegate rValueChangedDelegate;
    public Slider sR;
    public OnSliderValueChangedDelegate gValueChangedDelegate;
    public Slider sG;
    public OnSliderValueChangedDelegate bValueChangedDelegate;
    public Slider sB;

    public OnSliderValueChangedDelegate maxIterValueChangedDelegate;
    public Slider sMaxIter;
    public OnSliderValueChangedDelegate animSpeedValueChangedDelegate;
    public Slider sAnimSpeed;
    public OnSliderValueChangedDelegate rotRadiusValueChangedDelegate;
    public Slider sRotRadius;

    public delegate void OnEdValueChangedDelegate(InputField change);
    public OnEdValueChangedDelegate edCRValueChangedDelegate;
    public InputField edCR;
    public OnEdValueChangedDelegate edCIValueChangedDelegate;
    public InputField edCI;

    public delegate void OnCheckBoxValueChangedDelegate(Toggle toggle);
    public OnCheckBoxValueChangedDelegate cbUseTentacleRenderDelegate;
    public Toggle cbUseTentacleRender;

    public GameObject uiPanel;

    // Start is called before the first frame update
    void Start()
    {
        cbFractal.onValueChanged.AddListener(delegate {
            cbFractalValueChangedDelegate(cbFractal);
        });
        cbPreset.onValueChanged.AddListener(delegate {
            cbPresetValueChangedDelegate(cbPreset);
        });

        sSaturation.onValueChanged.AddListener(delegate {
            saturationValueChangedDelegate(sSaturation);
        });
        sR.onValueChanged.AddListener(delegate {
            rValueChangedDelegate(sR);
        });
        sG.onValueChanged.AddListener(delegate {
            gValueChangedDelegate(sG);
        });
        sB.onValueChanged.AddListener(delegate {
            bValueChangedDelegate(sB);
        });

        sMaxIter.onValueChanged.AddListener(delegate {
            maxIterValueChangedDelegate(sMaxIter);
        });
        sAnimSpeed.onValueChanged.AddListener(delegate {
            animSpeedValueChangedDelegate(sAnimSpeed);
        });

        sRotRadius.onValueChanged.AddListener(delegate {
            rotRadiusValueChangedDelegate(sRotRadius);
        });
        edCR.onValueChanged.AddListener(delegate {
            edCRValueChangedDelegate(edCR);
        });
        edCI.onValueChanged.AddListener(delegate {
            edCIValueChangedDelegate(edCI);
        });
        cbUseTentacleRender.onValueChanged.AddListener(delegate {
            cbUseTentacleRenderDelegate(cbUseTentacleRender);
        });
    }
     
    // Update is called once per frame
    void Update()
    {
        
    }
}
