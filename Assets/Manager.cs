﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UIManager;

public class FractPreset
{
    public Vector4 customCPoint = new Vector4(0.0f, 0.0f, 0.0f, 0.0f);
    public float cPointRotationRadius = 0.0f;
}

public class Manager : MonoBehaviour
{
    public Material mat;
    public UIManager uiManager;
        
    public Vector2 pos;
    public int maxIter = 155;
    public float scale = 4.0f;
    public float angle = 0.0f;
    public Vector4 color = new Vector4(0.6f, 0.4f, 0.2f, 1f);
    public float colorModifier = 10f;
    public float animSpeed = 0.5f;
    public float symmetry = 0f;
    public Vector4 customCPoint = new Vector4(0.0f, 0.0f, 0.0f, 0.0f);
    public float cPointRotationRadius = 0.7885f;
    public bool isUseTentacleRender = false;

    private Vector2 smoothPos;
    private float smoothScale;
    private float smoothAngle;

    private int selectedFractal = 0;

    private List<FractPreset> presets = new List<FractPreset>();
    

    void Start()
    {
        smoothScale = scale;
        smoothAngle = angle;
        smoothPos   = pos;

        uiManager.cbFractalValueChangedDelegate = cbFractalValueChanged;
        uiManager.saturationValueChangedDelegate= saturationValueChanged;
        uiManager.rValueChangedDelegate         = rValueChanged;
        uiManager.gValueChangedDelegate         = gValueChanged;
        uiManager.bValueChangedDelegate         = bValueChanged;
        uiManager.maxIterValueChangedDelegate   = maxIterValueChanged;
        uiManager.animSpeedValueChangedDelegate = animSpeedValueChanged;
        uiManager.rotRadiusValueChangedDelegate = rotRadiusValueChanged;
        uiManager.cbPresetValueChangedDelegate  = cbPresetValueChanged;
        uiManager.edCRValueChangedDelegate = edCRValueChanged;
        uiManager.edCIValueChangedDelegate = edCIValueChanged;
        uiManager.cbUseTentacleRenderDelegate = cbUseTentacleRenderChanged;

        initPresets();
    }

    private void initPresets()
    {
        FractPreset fractPreset = new FractPreset();
        fractPreset.cPointRotationRadius = 0.7885f;
        fractPreset.customCPoint.x = 0.0f;
        fractPreset.customCPoint.y = 0.0f;
        presets.Add(fractPreset);

        fractPreset = new FractPreset();
        fractPreset.cPointRotationRadius = 0.0f;
        fractPreset.customCPoint.x = -0.8f;
        fractPreset.customCPoint.y = 0.156f;
        presets.Add(fractPreset);

        fractPreset = new FractPreset();
        fractPreset.cPointRotationRadius = 0.0f;
        fractPreset.customCPoint.x = 0.285f;
        fractPreset.customCPoint.y = 0.01f;
        presets.Add(fractPreset);

        fractPreset = new FractPreset();
        fractPreset.cPointRotationRadius = 0.0f;
        fractPreset.customCPoint.x = -0.0085f;
        fractPreset.customCPoint.y = 0.71f;
        presets.Add(fractPreset);
    }

    private void selectPreset(int index)
    {
        if (index < 0 || index >= presets.Count) return;

        FractPreset selectedPreset = presets[index];
        customCPoint = selectedPreset.customCPoint;
        cPointRotationRadius = selectedPreset.cPointRotationRadius;
        uiManager.edCR.text = customCPoint.x.ToString();
        uiManager.edCI.text = customCPoint.y.ToString();
        uiManager.sRotRadius.value = cPointRotationRadius;
    }

    void cbUseTentacleRenderChanged(Toggle toggle)
    {
        isUseTentacleRender = toggle.isOn;
    }

    void cbFractalValueChanged(Dropdown change)
    {
        selectedFractal = change.value;        
    }
    void cbPresetValueChanged(Dropdown change)
    {
        int presetIndex = change.value;
        selectPreset(presetIndex);
    }
    void saturationValueChanged(Slider change)
    {
        colorModifier = change.value;
    }
    void rValueChanged(Slider change)
    {
        color.x = change.value;
    }
    void gValueChanged(Slider change)
    {
        color.y = change.value;
    }
    void bValueChanged(Slider change)
    {
        color.z = change.value;
    }
    void maxIterValueChanged(Slider change)
    {
        maxIter = (int)change.value;
    }
    void animSpeedValueChanged(Slider change)
    {
        animSpeed = change.value;
    }
    void rotRadiusValueChanged(Slider change)
    {
        cPointRotationRadius = change.value;
    }
    void edCRValueChanged(InputField change)
    {
        string strVal = change.text;
        float val = 0.0f;
        if (!float.TryParse(strVal.Replace('.', ','), out val)) return;

        customCPoint.x = val;
    }
    void edCIValueChanged(InputField change)
    {
        string strVal = change.text;
        float val = 0.0f;
        if (!float.TryParse(strVal.Replace('.', ','), out val)) return;

        customCPoint.y = val;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        handleInputs();
        updateShader();
    }

    private void handleInputs()
    {
        if (Input.GetKey(KeyCode.Q)) angle += 0.01f;        
        if (Input.GetKey(KeyCode.E)) angle -= 0.01f;

        Vector2 dir = new Vector2(0.01f * scale, 0);
        float s = Mathf.Sin(angle);
        float c = Mathf.Cos(angle);
        dir = new Vector2(dir.x * c, dir.x * s);

        if (Input.mouseScrollDelta.y > 0) scale *= 0.95f;
        if (Input.mouseScrollDelta.y < 0) scale *= 1.05f;

        if (Input.GetKey(KeyCode.A)) pos -= dir;
        if (Input.GetKey(KeyCode.D)) pos += dir;

        dir = new Vector2(-dir.y, dir.x);
        if (Input.GetKey(KeyCode.W)) pos += dir;
        if (Input.GetKey(KeyCode.S)) pos -= dir;

        if (Input.GetKeyUp(KeyCode.Z))
        {
            if (symmetry > 0.1f) symmetry = 0.0f;
            else symmetry = 1.0f;
        }

        if (Input.GetKeyUp(KeyCode.F1))
        {     
            if (uiManager.uiPanel == null) return;
            uiManager.uiPanel.SetActive(!uiManager.uiPanel.activeSelf);
        }

    }

    private void updateShader()
    {
        smoothPos   = Vector2.Lerp(smoothPos, pos,   0.1f);
        smoothScale = Mathf.Lerp(smoothScale, scale, 0.02f);
        smoothAngle = Mathf.Lerp(smoothAngle, angle, 0.02f);
        float aspect = Screen.width / (float)Screen.height;
        float scaleX = smoothScale;
        float scaleY = smoothScale;
        if (aspect > 1f) scaleY /= aspect;
        else scaleX *= aspect;

        mat.SetVector("_Area", new Vector4(smoothPos.x, smoothPos.y, scaleX, scaleY));
        mat.SetFloat("_Angle", smoothAngle);
        mat.SetInt("_MaxIter", maxIter);
        mat.SetInt("_SelectedFractal", selectedFractal);
        mat.SetVector("_Color", color);
        mat.SetFloat("_Repeat", colorModifier);
        mat.SetFloat("_AnimSpeed", animSpeed);
        mat.SetFloat("_Symmetry", symmetry);
        mat.SetFloat("_RotationRadius", cPointRotationRadius);
        mat.SetVector("_CustomCPoint", customCPoint);
        mat.SetFloat("_UseTentacleRender", isUseTentacleRender ? 1.0f : 0.0f);
    }
}
