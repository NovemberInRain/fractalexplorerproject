﻿Shader "Explorer/MandelbrotModify"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Area("Area", vector) = (0,0,4,4)
		_Angle("Angle", range(-3.1415, 3.1415)) = 0

		_MaxIter("MaxIter", range(0, 1024)) = 155
		_SelectedFractal("SelectedFractal", range(0, 10)) = 0
		_Color("Color", vector) = (0.6, 0.4, 0.2, 1)
		_Repeat("Repeat", range(0, 1000)) = 10
		_Symmetry("Symmetry", range(0, 1)) = 0

		_AnimSpeed("AnimSpeed", range(0.1, 100)) = 0.5
		_UseTentacleRender("UseTentacleRender", float) = 1.0
		
		_RotationRadius("RotationRadius", range(0, 10)) = 0.7885
		_CustomCPoint("CustomCPoint", vector) = (0.0, 0.0, 0.0, 0.0)
	}
		SubShader
		{
			// No culling or depth
			Cull Off ZWrite Off ZTest Always

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				float4 _Area;
				float _Angle;
				float _MaxIter;
				int _SelectedFractal;
				float4 _Color;
				float _Repeat;
				float _AnimSpeed;
				float _Symmetry;
				float _RotationRadius;
				float2 _CustomCPoint;
				float _UseTentacleRender;
				
				float2 rot(float2 p, float2 pivot, float a)
				{
					float s = sin(a);
					float c = cos(a);
					p -= pivot;
					p = float2(p.x * c - p.y * s, p.x * s + p.y * c);
					p += pivot;

					return p;
				}

				float2 getArea(v2f i)
				{
					float2 uv = i.uv - 0.5;
					uv = abs(uv);
					uv = rot(uv, 0, 0.25 * 3.1415);
					uv = abs(uv);
					uv = lerp(i.uv - 0.5, uv, _Symmetry);

					float2 c = _Area.xy + uv * _Area.zw;
					c = rot(c, _Area.xy, _Angle);
					return c;
				}

				float4 calcMandelbrot(float2 c, float r)
				{					
					float r2 = r * r;

					float iter = 0.0;
					float2 z = float2(0.0, 0.0);;
					float2 zPrev = z;					
					while (dot(z, zPrev) < r2 && iter < _MaxIter)					
					{	
						zPrev = z;
						z = float2(z.x * z.x - z.y * z.y, 2 * z.x * z.y) + c;

						if (_UseTentacleRender > 0.5)	zPrev = rot(zPrev, 0, _Time.y * _AnimSpeed * 0.5);
						else							zPrev = z;
						
						iter += 1.0;
					}
					float dist = length(z);
					return float4(iter, dist, log2(log(dist) / log(r)), atan2(z.x, z.y));
				}

				float4 calcJulia(float2 zStart, float r)
				{
					float r2 = r * r;

					float iter = 0.0;
					float2 z = zStart;
					float2 zPrev = z;

					float cRad = _RotationRadius;
					float2 c = float2(cRad * cos(_Time.y * _AnimSpeed * 0.1), cRad * sin(_Time.y * _AnimSpeed * 0.1));
					if(length(_CustomCPoint) > 0.1) c = float2(_CustomCPoint.x, _CustomCPoint.y);
									   					
					while (dot(z, zPrev) < r2 && iter < _MaxIter )
					{
						zPrev = z;
						z = float2(z.x * z.x - z.y * z.y, 2 * z.x * z.y) + c;

						if (_UseTentacleRender > 0.5)	zPrev = rot(zPrev, 0, _Time.y * _AnimSpeed * 0.5);
						else							zPrev = z;

						iter += 1.0;						
					}
					float dist = length(z);
					return float4(iter, dist, log2(log(dist) / log(r)), atan2(z.x, z.y));
				}

				float4 calcBurningShip(float2 c, float r)
				{
					float r2 = r * r;

					float iter = 0.0;
					float2 z = float2(0.0, 0.0);
					float2 zPrev = z;
					while (dot(z, zPrev) < r2 && iter < _MaxIter)
					{
						zPrev = z;

						float xtemp = z.x * z.x - z.y * z.y + c.x;
						z.y = abs(2 * z.x * z.y + c.y);
						z.x = abs(xtemp);

						if (_UseTentacleRender > 0.5)	zPrev = rot(zPrev, 0, _Time.y * _AnimSpeed * 0.5);
						else							zPrev = z;

						iter += 1.0;
					}
					float dist = length(z);
					return float4(iter, dist, log2(log(dist) / log(r)), atan2(z.x, z.y));
				}

				float4 calcTricorn(float2 c, float r)
				{
					float r2 = r * r;

					float iter = 0.0;
					float2 z = float2(0.0, 0.0);
					float2 zPrev = z;
					while (dot(z, zPrev) < r2 && iter < _MaxIter)
					{
						zPrev = z;

						float xtemp = z.x * z.x - z.y * z.y + c.x;
						z.y = -2.0 * z.x * z.y + c.y;
						z.x = xtemp;

						if (_UseTentacleRender > 0.5)	zPrev = rot(zPrev, 0, _Time.y * _AnimSpeed * 0.5);
						else							zPrev = z;

						iter += 1.0;
					}
					float dist = length(z);
					return float4(iter, dist, log2(log(dist) / log(r)), atan2(z.x, z.y));
				}
				
				fixed4 frag(v2f i) : SV_Target
				{
					float2 c = getArea(i);
					float4 fractResult = float4(0.0, 0.0, 0.0, 0.0);
					if (_SelectedFractal == 0) fractResult = calcMandelbrot(c, 20.0);
					if (_SelectedFractal == 1) fractResult = calcJulia(c, 20.0);
					if (_SelectedFractal == 2) fractResult = calcBurningShip(c, 5.0);
					if (_SelectedFractal == 3) fractResult = calcTricorn(c, 5.0);

					float iter = fractResult.x;
					if (iter >= _MaxIter) return 0;

					float dist = fractResult.y;
					float fracIter = fractResult.z;
					iter -= fracIter;
					float m = sqrt(iter / _MaxIter);
					float4 col = sin(float4(_Color.x, _Color.y, _Color.z, 1) * m * _Repeat + _Time.y * _AnimSpeed) * 0.5 + 0.5;

					if (_UseTentacleRender > 0.5)
					{
						float angle = fractResult.w;
						col *= smoothstep(3, 0, fracIter) ;
						col *= 1 + sin(angle * 2 + _Time.y * _AnimSpeed * 2) * 0.2;
					}
					return col;
				}
				ENDCG
			}
		}
}